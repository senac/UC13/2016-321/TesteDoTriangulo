package br.com.senac.testedotriangulo;

import javafx.scene.shape.TriangleMesh;
import org.junit.Assert;
import org.junit.Test;

public class TestadorDeTriangulo {

    @Test
    public void testEhUmTriangulo() {
        Triangulo triangulo = new Triangulo(10, 5, 5);
        Assert.assertTrue(triangulo.isTriangulo());
    }

    @Test
    public void testNaoEhTriangulo() {
        Triangulo triangulo = new Triangulo(10, 100, 1);
        Assert.assertFalse(triangulo.isTriangulo());
    }

    @Test
    public void ehEquilatero() {
        Triangulo triangulo = new Triangulo(10, 10, 10);
        Assert.assertTrue(triangulo.isEquilatero());
    }

    @Test
    public void ehIsoceles() {
        Triangulo triangulo = new Triangulo(10, 5, 10);
        Assert.assertTrue(triangulo.isIsoceles());
    }

    @Test
    public void ehEscaleno() {
        Triangulo triangulo = new Triangulo(1, 100, 10);
        Assert.assertTrue(triangulo.isEscaleno());
    }

}
